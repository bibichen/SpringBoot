package com.yobing.hellodemo.controller;

import com.yobing.hellodemo.entity.DemoEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

/**
 * @author : yobing Chen
 * @Data : 2017/4/6
 */
@Controller
@RequestMapping("/templates")
public class TemplatesController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 映射地址是：/templates/hello
     * @return
     */
    @RequestMapping("/hello")
    public String hello(Map<String,Object> map) {
        //日志级别从低到高分为TRACE < DEBUG < INFO < WARN < ERROR < FATAL，如果设置为WARN，则低于WARN的信息都不会输出。
        logger.trace("日志输出 trace");
        logger.debug("日志输出 debug");
        logger.info("日志输出 info");
        logger.warn("日志输出 warn");
        logger.error("日志输出 error");
        map.put("name", "悟空");
        return "hello";
    }
    @RequestMapping("/hello1")
    public ModelAndView hello1() {
        //返回的是ModelAndView
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("name", "tom");
        ModelAndView mv = new ModelAndView("hello");
        //如果map是作为方法参数出入进来的可以不用addAllObjects
        mv.addAllObjects(map);
        return mv;
    }

    @RequestMapping("/hello2")
    public ModelAndView hello2() {
        List<DemoEntity> list = new ArrayList<DemoEntity>();
        DemoEntity bean = new DemoEntity(1,"测试1",new Date());
        list.add(bean);
        bean = new DemoEntity(2, "测试2", new Date());
        list.add(bean);
        bean = new DemoEntity(3, "测试3", new Date());
        list.add(bean);
        bean = new DemoEntity(4, "测试4", new Date());
        list.add(bean);
        bean = new DemoEntity(5, "测试5", new Date());
        list.add(bean);
        ModelAndView mv = new ModelAndView("index");
        mv.addObject("learnList", list);
        return mv;
    }

    @RequestMapping("/helloFtl")
    public String helloFtl(Map<String,Object> map) {
        map.put("name", "豆豆");
        return "helloftl";
    }

}
