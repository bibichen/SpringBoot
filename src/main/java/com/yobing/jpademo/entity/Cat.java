package com.yobing.jpademo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 实体类
 * 1.使用@Entity进行实体类的持久化操作，当JPA检测到我们的实体类当中有@Entity注解时，会在数据库中生存对应的表结构信息。
 *
 * 2.使用@Id指定主键
 *
 * @author : yobing Chen
 * @Data : 2017/3/29
 */
@Entity
public class Cat {

    /**
     * 使用@Id指定主键
     * 使用代码@GeneratedValue(strategy=GenerationType.AUTO)
     * 指定主键的生成策略，mysql默认的是自增长。
     */
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;//主键
    private String catName;//姓名 cat_name
    private int catAge;//年龄 cat_age

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public int getCatAge() {
        return catAge;
    }

    public void setCatAge(int catAge) {
        this.catAge = catAge;
    }
}
