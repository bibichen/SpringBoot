package com.yobing.jpademo.service;

import com.yobing.jpademo.entity.Cat;
import com.yobing.jpademo.repository.CatRepository;
import com.yobing.jpademo.repository.MyRepository;
import com.yobing.jpademo.repository.PageRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;

/**
 * @author : yobing Chen
 * @Data : 2017/3/29
 */
@Service
public class CatService {

    @Resource
    private CatRepository catRepository;
    @Resource
    private MyRepository myRepository;
    @Resource
    private PageRepository pageRepository;
    /**
     * save,update,delete方法需要绑定事物
     * 使用@Transactional进行事物的绑定
     * @param cat
     */
    @Transactional
    public void save(Cat cat) {
        catRepository.save(cat);
    }
    @Transactional
    public void delete(int id) {
        catRepository.delete(id);
    }
    public Iterable<Cat> getAll(){
        return catRepository.findAll();
    }

    public Cat findByCatName(String catName) {
        return myRepository.findByCatName(catName);
    }
    public Cat findMyCatName(String catName) {
        return myRepository.findMyCatName(catName);
    }

}
