package com.yobing.jpademo.repository;

import com.yobing.jpademo.entity.Cat;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository --是接口，不是class
 * @author : yobing Chen
 * @Data : 2017/3/29
 */
public interface CatRepository extends CrudRepository<Cat,Integer> {
}
