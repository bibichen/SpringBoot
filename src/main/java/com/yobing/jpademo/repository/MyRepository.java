package com.yobing.jpademo.repository;

import com.yobing.jpademo.entity.Cat;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

/**
 * @author : yobing Chen
 * @Data : 2017/3/30
 */
public interface MyRepository extends Repository<Cat,Integer>{

    /**
     * 1.查询方法以get、find、read开头
     * 2.涉及查询条件时，条件的属性用条件关键字连接，要注意的是条件属性以首字母大写。
     */
    //根据catName进行查询
    public Cat findByCatName(String catName);

    /**
     * 如何编写JPQL语句
     * Hibernate -- HQL语句
     * JPQL 语句和 HQL语句是类似的
     */
    @Query("from Cat where catName=:cn")
    public Cat findMyCatName(@Param("cn")String catName);

}
