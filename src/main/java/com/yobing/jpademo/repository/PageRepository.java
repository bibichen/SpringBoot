package com.yobing.jpademo.repository;

import com.yobing.jpademo.entity.Cat;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author : yobing Chen
 * @Data : 2017/3/30
 */
public interface PageRepository extends PagingAndSortingRepository<Cat,Integer> {
}
