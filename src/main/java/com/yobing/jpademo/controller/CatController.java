package com.yobing.jpademo.controller;

import com.yobing.jpademo.entity.Cat;
import com.yobing.jpademo.service.CatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : yobing Chen
 * @Data : 2017/3/29
 */
@RestController
@RequestMapping("/cat")
public class CatController {
    @Autowired
    private CatService catService;

    @RequestMapping("/save")
    public String save() {
        Cat cat = new Cat();
        cat.setCatName("Tom");
        cat.setCatAge(3);
        catService.save(cat);
        return "save ok";
    }
    @RequestMapping("/delete")
    public String delete() {
        catService.delete(1);
        return "delete ok";
    }
    @RequestMapping("/getAll")
    public Iterable<Cat> getAll() {
        return catService.getAll();
    }
    @RequestMapping("/findByCatName")
    public Cat findByCatName(String catName) {
        return catService.findByCatName(catName);
    }
    @RequestMapping("/findMyCatName")
    public Cat findMyCatName(String catName) {
        System.out.println("执行/findMyCatName");
        return catService.findMyCatName(catName);
    }

}
