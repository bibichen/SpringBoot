package com.yobing.jdbctemplatedemo.controller;

import com.yobing.jdbctemplatedemo.entity.Panda;
import com.yobing.jdbctemplatedemo.service.PandaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : yobing Chen
 * @Data : 2017/4/13
 */
@RestController
@RequestMapping("/panda")
public class PandaController {
    @Autowired
    private PandaService pandaService;

    @RequestMapping("/add")
    public String addPanda() {
        Panda panda = new Panda();
        panda.setName("熊猫");
        panda.setAge(3);
        pandaService.add(panda);
        return "添加成功";
    }

}
