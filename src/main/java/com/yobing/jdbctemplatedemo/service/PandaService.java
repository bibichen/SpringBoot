package com.yobing.jdbctemplatedemo.service;

import com.yobing.jdbctemplatedemo.entity.Panda;

/**
 * @author : yobing Chen
 * @Data : 2017/4/13
 */
public interface PandaService {
    int add(Panda panda);

    Panda findById(int id);
}
