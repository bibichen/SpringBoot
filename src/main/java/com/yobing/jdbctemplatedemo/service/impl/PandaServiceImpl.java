package com.yobing.jdbctemplatedemo.service.impl;

import com.yobing.jdbctemplatedemo.dao.PandaDao;
import com.yobing.jdbctemplatedemo.entity.Panda;
import com.yobing.jdbctemplatedemo.service.PandaService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author : yobing Chen
 * @Data : 2017/4/13
 */
@Service
public class PandaServiceImpl implements PandaService {

    @Resource
    private PandaDao pandaDao;

    @Override
    //@Transactional
    public int add(Panda panda) {
        return pandaDao.add(panda);
    }

    @Override
    public Panda findById(int id) {
        return pandaDao.findById(id);
    }
}
