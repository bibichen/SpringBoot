package com.yobing.jdbctemplatedemo.dao.impl;

import com.yobing.jdbctemplatedemo.dao.PandaDao;
import com.yobing.jdbctemplatedemo.entity.Panda;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * @author : yobing Chen
 * @Data : 2017/4/13
 */
@Repository
public class PandaDaoImpl implements PandaDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int add(Panda panda) {
        return jdbcTemplate.update("INSERT INTO panda(name,age) VALUES (?,?)", panda.getName(), panda.getAge());
    }

    @Override
    public int update(Panda panda) {
        return 0;
    }

    @Override
    public Panda findById(int id) {
        return null;
    }
}
