package com.yobing.jdbctemplatedemo.dao;

import com.yobing.jdbctemplatedemo.entity.Panda;

/**
 * @author : yobing Chen
 * @Data : 2017/4/13
 */
public interface PandaDao {
    int add(Panda panda);
    int update(Panda panda);

    Panda findById(int id);
}
